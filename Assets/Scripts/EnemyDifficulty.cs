using UnityEngine;
[CreateAssetMenu]
public class EnemyDifficulty : ScriptableObject
{
    [SerializeField] private float _baseHealth, _baseSpeed,_maxSpeed, _baseTimeToRespawn, _minTimeToRespawn;
    [SerializeField] private float _stepToIncreaseHealth, _stepToIncreaseSpeed, _stepToReduceRespawn;
    public float GetHealthValue() => _baseHealth;

    public float GetSpeedValue()=> _baseSpeed;

    public float GetReducedTimeToRespawn() => _baseTimeToRespawn;

    public void IncreaseAllparameters()
    {
        IncreaseAdditionalHealth();
        IncreaseAdditionalSpeed();
        ReduceTimeToRespawn();
    }

    private void IncreaseAdditionalHealth()=> _baseHealth += _stepToIncreaseHealth;

    private void IncreaseAdditionalSpeed()
    {
        _baseSpeed+=_stepToIncreaseSpeed;
        _baseSpeed = Mathf.Clamp(_baseSpeed, 0, _maxSpeed); 
    }

    private void ReduceTimeToRespawn()
    {
        if(_baseTimeToRespawn > _minTimeToRespawn)
        _baseTimeToRespawn -= _stepToReduceRespawn;
    }
}
