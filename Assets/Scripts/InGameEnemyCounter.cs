using TMPro;
using UnityEngine;

public class InGameEnemyCounter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _uiCountText;
    [SerializeField] private GameOver _gameOver;
    [SerializeField] private DifficultyChanger _difficultyCounter;
    private int _enemyCount = 0;
    private int _enemiesToGameOver = 10;

    public void IncreaseEnemyCount()
    {
        _enemyCount++;
        UpdateUICountText();
        if (_enemyCount >= _enemiesToGameOver)
        {
            _gameOver.EndGame();
        }
    }

    public void DecreaseEnemyCount()
    {
        _enemyCount--;
        UpdateUICountText();
        _difficultyCounter.CountDownEnemies();
    }

    private void UpdateUICountText()
    {
        _uiCountText.SetText($"������ �� ����: {_enemyCount} ");
    }
}
