using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float _timeReturnToPull;
    [SerializeField] private Animator _animator;
    [SerializeField] private EnemyMoving[] _enemyMovings;
    [SerializeField] private AnimationClip hit, die;
    private bool _isDead;
    private Arrow _arrow;
    private Health _health = new Health(0);
    private Speed _speed = new Speed(0);
    private Coroutine _respawn;
    private EnemyMoving _enemyMoving;
    public ObjectPool ObjectPool { get; set; }
    public InGameEnemyCounter EnemyCounter { get; set; }
    public Animator Animator { get => _animator; }
    public Health Health { get => _health;}
    public Speed Speed { get => _speed;}
    public EnemyMoving EnemyMoving { get => _enemyMoving; set => _enemyMoving = value; }
    public Arrow Arrow { get => _arrow; set => _arrow = value; }

    private void Awake()
    {
        _enemyMovings = new EnemyMoving[] { new CircleMoving(), new LineMoving(), new SquareMoving() };
    }

    private void Start()
    {
        EnemyMoving = RandomPickMovement();
    }

    private void Update()
    {
       if(!_isDead) EnemyMoving.Moving(transform,_speed.GetStat());
    }

    public void ApplyDamage()
    {   
        Animator.Play(hit.name);
        Health.DecreaseStat(1f);
        if(Health.GetStat()<=0)
        {   
            EnemyDied();
        }
    }

    private EnemyMoving RandomPickMovement()
    {
        return _enemyMovings[Random.Range(0, _enemyMovings.Length)];
    }

    private void EnemyDied()
    {
        _isDead = true;
        Animator.Play(die.name);
        EnemyCounter.DecreaseEnemyCount();
        RespawnEnemy();
    }
   
    private void RespawnEnemy()
    {
        _respawn = StartCoroutine(DelayEnemySpawn());
    }

    private IEnumerator DelayEnemySpawn()
    {
        yield return new WaitForSeconds(_timeReturnToPull);
        _arrow.ReturnToPull();
        _isDead = false;
        ObjectPool.ReturnObjectToPool(gameObject);
        StopCoroutine(_respawn);
    }
}
