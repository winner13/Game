
public abstract class Stat 
{
    protected float _value = 0f;

    protected Stat(float value)
    {
        _value = value;
    }

    public float GetStat()=>_value;

    public void SetStat(float value)
    {
        if (value < 0)
        {
            throw new System.NotImplementedException("negative value");
        }
        else _value = value;
    }

    public abstract void IncreaseStat(float value);

    public abstract void DecreaseStat(float value);
}
