﻿
public class Health : Stat
{
    public Health(float value) : base(value) {}

    public override void IncreaseStat(float value)
    {
        if (value < 0)
        {
            throw new System.NotImplementedException("negative value");
        }
        else _value += value;
    }

    public override void DecreaseStat(float value)
    {
        _value -= value;
    }
}
