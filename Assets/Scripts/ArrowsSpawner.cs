using UnityEngine;

public class ArrowsSpawner : MonoBehaviour
{
    [SerializeField] private Transform _startPointOfDistance;
    [SerializeField] private ObjectPool _arrowPool;
    [SerializeField] private CapsuleCollider _capsuleCollider;

    public Arrow AddArrow(Transform target)
    {
        GameObject arrowObject = _arrowPool.ActivateObjectFromPull(FindRespawnPlace.GetRespawnPlace(), Quaternion.identity);
        Arrow arrow = arrowObject.GetComponent<Arrow>();
        arrow.SetStartPointOfDistance(_startPointOfDistance);
        arrow.SetTarget(target);
        arrow.SetCapsuleCollider(_capsuleCollider);
        arrow.SetArrowPool(_arrowPool);
        return arrow;
    }
}
