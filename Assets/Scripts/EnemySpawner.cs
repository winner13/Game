using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private SpawnTime spawnTime;
    [SerializeField] private float minTimeToRespawn, maxTimeToRespawn;
    [SerializeField] private EnemySettings enemySettings;
    [SerializeField] private ArrowsSpawner enemyPointers;
    private Coroutine enemySpawner;

    private void Awake()
    {
        spawnTime = new SpawnTime(maxTimeToRespawn);
    }

    public void Start()
    {
        if (enemySpawner == null)
        {
            enemySpawner = StartCoroutine(ActivateEnemy());
        }
    }

    public void ChangeSpawnTime(float newTime)
    {
        spawnTime.DecreaseStat(newTime);
        spawnTime.SetStat(Mathf.Clamp(spawnTime.GetStat(), minTimeToRespawn, maxTimeToRespawn));
    }

    private void SpawnEnemy()
    {
        GameObject enemyObject = enemySettings.EnemyPool.ActivateObjectFromPull(FindRespawnPlace.GetRespawnPlace(), Quaternion.identity);
        Enemy enemy = enemyObject.GetComponent<Enemy>();
        Arrow arrow = enemyPointers.AddArrow(enemyObject.transform);
        enemy.Arrow = arrow;
        if (enemy != null)
        {
            enemySettings.SetEnemySettings(enemy);
        }
        else throw new System.NotImplementedException("enemy is null");
    }

    IEnumerator ActivateEnemy()
    {
        while (true)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(spawnTime.GetStat());
        }
    }
}


