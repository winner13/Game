using System.Collections.Generic;
using UnityEngine;

public class FindRespawnPlace : MonoBehaviour
{
    [SerializeField] private Transform _ground;
    public static FindRespawnPlace instance;

    public void Awake()
    {
       if(instance ==null)
        {
            instance = this;
        }
    }

    public static Vector3 GetRespawnPlace()
    {
        List<Vector3> VerticeList = new List<Vector3>(instance._ground.GetComponent<MeshFilter>().sharedMesh.vertices);
        Vector3 leftTop = instance._ground.transform.TransformPoint(VerticeList[0]);
        Vector3 rightTop = instance._ground.transform.TransformPoint(VerticeList[10]);
        Vector3 leftBottom = instance._ground.transform.TransformPoint(VerticeList[110]);
        Vector3 planeWidth = rightTop - leftTop;
        Vector3 planeHeight = leftBottom - leftTop;
        return new Vector3(Random.Range(-planeWidth.magnitude / 2.5f, planeWidth.magnitude / 2.5f), 0, Random.Range(-planeHeight.magnitude / 2.5f, planeHeight.magnitude / 2.5f));
    }
}
