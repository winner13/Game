using UnityEngine;

public class TouchInput : MonoBehaviour
{
    [SerializeField] private Camera _cameraMain;
    [SerializeField] private LayerMask _layerMask;
    [SerializeField] private float _moveSpeed = 1;
    [SerializeField] private float _radius = 10;
    
    public void Update()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touchCount == 1)
            {
                Touch currentTouch = Input.GetTouch(0);

                if (currentTouch.phase == TouchPhase.Moved)
                {
                    Scrolling(currentTouch);
                }

                if (currentTouch.phase == TouchPhase.Began)
                {
                 Ray ray = _cameraMain.ScreenPointToRay(Input.GetTouch(0).position);
                 bool hit = Physics.Raycast(ray, out RaycastHit raycastHit, 100f, _layerMask);

                 if (hit)
                 {
                    Enemy enemy = raycastHit.collider.GetComponentInParent<Enemy>();

                    if (enemy && enemy.Health.GetStat()>0)
                    {
                            enemy.ApplyDamage();
                    }
                 }
                }
            }
            
        }
    }

    private void Scrolling(Touch currentTouch)
    {
        Vector3 movePosition = new Vector3(
         _cameraMain.transform.position.x + currentTouch.deltaPosition.x * _moveSpeed * -1 * Time.deltaTime,
         _cameraMain.transform.position.y,
         _cameraMain.transform.position.z + currentTouch.deltaPosition.y * _moveSpeed * -1 * Time.deltaTime);

        Vector3 distance = movePosition - _cameraMain.transform.position;

        if (distance.magnitude < _radius)
            _cameraMain.transform.position = movePosition;
    }




}
