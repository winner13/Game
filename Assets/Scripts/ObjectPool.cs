﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private int countObjects =10;
    [SerializeField] private GameObject[] objectToPool;
    private List<GameObject> pull = new List<GameObject>();
    public List<GameObject> Pull { get => pull; set => pull = value; }

    public void Awake()
    {   
        if(objectToPool.Length == 0)
        { Debug.LogWarning("Pull error", this);return; }
        for (int i = 0; i < countObjects; i++)
        {
            RandomObjectSpawnAndAddToPull();
            Pull[i].SetActive(false);
        }
    }

    public GameObject ActivateObjectFromPull(Vector3 position, Quaternion rotation)
    {
        if (Pull.Count == 0)
        {
            RandomObjectSpawnAndAddToPull();
        }
        GameObject firstPullObject = Pull[0];
        firstPullObject.transform.position = position;
        firstPullObject.transform.rotation = rotation;
        firstPullObject.SetActive(true);
        Pull.RemoveAt(0);
        return firstPullObject;
    }

    public void ReturnObjectToPool(GameObject gameObject)
    {
        gameObject.SetActive(false);
        Pull.Add(gameObject);
    }

    private void RandomObjectSpawnAndAddToPull()
    {
        int randomPick = Random.Range(0, objectToPool.Length);
        Pull.Add(Instantiate(objectToPool[randomPick], transform));
    }
}
