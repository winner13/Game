using UnityEngine;
using TMPro;
public class DifficultyChanger : MonoBehaviour
{
    [SerializeField] private EnemyDifficulty _enemyDifficulty;
    [SerializeField] private int _enemiesNeededToNextDifficult;
    [SerializeField] private TextMeshProUGUI _countWindow;
    [SerializeField] private EnemySpawner _enemySpawner;
    private int _remainingEnemies;

    private void Awake()
    {
        _remainingEnemies = _enemiesNeededToNextDifficult;
        UpdateRemainingEnemies();
    }

    public void CountDownEnemies()
    {
        _remainingEnemies--;
        if (_remainingEnemies <= 0)
        {
            _remainingEnemies = _enemiesNeededToNextDifficult;
            IncreaseDifficulty();
        }
        UpdateRemainingEnemies();
    }

    private void IncreaseDifficulty()
    {
        _enemySpawner.ChangeSpawnTime(_enemyDifficulty.GetReducedTimeToRespawn());
        _enemyDifficulty.IncreaseAllparameters();
        _remainingEnemies = _enemiesNeededToNextDifficult;
    }

    private void UpdateRemainingEnemies()=> _countWindow.SetText($"����� �� ��������� ���������: {_remainingEnemies} ");
}
