using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSize : MonoBehaviour
{
    [SerializeField] private RectTransform[] _rectTransforms;
    [SerializeField] private Vector2 _minAnchorForButtons = new Vector2(0.33f,0.2f);
    [SerializeField] private Vector2 _maxAnchorForButtons = new Vector2(0.66f,0.8f);
    [SerializeField] private float step;

    public void Start()
    {
        step = Mathf.Clamp(step, 0.01f, 0.1f);
        SetSize();
    }

    private void SetSize()
    {
        Vector2 vector2 = GetButtonSize();
        Vector2 newMinPos = _maxAnchorForButtons ;

        for (int i = 0; i < _rectTransforms.Length; i++)
        {
            _rectTransforms[i].anchorMax = newMinPos;
            Vector2 downleft = newMinPos - vector2;
            _rectTransforms[i].anchorMin = downleft;
            newMinPos = new Vector2(newMinPos.x, downleft.y-step);
        }
    }

    private Vector2 GetButtonSize()
    {
        int count = _rectTransforms.Length;
        float xSize = (_maxAnchorForButtons.x - _minAnchorForButtons.x);
        float ySize = (_maxAnchorForButtons.y - _minAnchorForButtons.y-step*_rectTransforms.Length) / count;
        return new Vector2(xSize, ySize);
    }
}
