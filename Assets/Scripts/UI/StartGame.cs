using UnityEngine;

public class StartGame : MonoBehaviour
{
    [SerializeField] private GameObject _menuUI,_inGameUI;

    private void Awake()
    {
        Time.timeScale = 0;
    }

    public void StartNewGame()
    {
        Time.timeScale = 1;
        _menuUI.SetActive(false);
        _inGameUI.SetActive(true);
    }
}
