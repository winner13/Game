﻿using UnityEngine;

public class SquareMoving : EnemyMoving
{
    public override void Moving(Transform transform, float speed)
    {
        _time += Time.deltaTime;
        transform.Translate(transform.forward * speed*Time.deltaTime, Space.World);
        if (_time >= 2f)
        {
            transform.Rotate(new Vector3(0, 90, 0));
            _time = 0;
        }
    }
}
