using UnityEngine;

public interface IEnemyMoving
{
    public void Moving(Transform transform, float speed);
}
