using UnityEngine;

public abstract class EnemyMoving : IEnemyMoving
{
    protected float _time;

    public abstract void Moving(Transform transform, float speed);
}
