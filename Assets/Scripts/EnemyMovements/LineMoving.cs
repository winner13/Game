﻿using UnityEngine;

public class LineMoving : EnemyMoving
{
    public override void Moving(Transform transform,float speed)
    {
        _time += Time.deltaTime;
        transform.Translate(transform.forward * speed* Time.deltaTime, Space.World);
        if (_time >= 2f)
        {
            transform.Rotate(new Vector3(0, 180, 0));
            _time = 0;
        }
    }
}
