﻿using UnityEngine;

public class CircleMoving : EnemyMoving
{
    public override void Moving(Transform transform, float speed)
    {
        _time += Time.deltaTime;
        transform.Translate(transform.forward * Time.deltaTime* speed, Space.World);
        if (_time >= 0.1f)
        {
            transform.Rotate(new Vector3(0, 1, 0));
            _time = 0;
        }
    }
}
