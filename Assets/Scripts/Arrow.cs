using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField] private float _minDistanceToDisable = 10f;
    private Transform _target, _startPointOfDistance;
    private ObjectPool _arrowPool;
    private CapsuleCollider _capsuleCollider;
    private void Update()
    {
        if (_target != null)
        {
            float dist = Vector3.Distance(_startPointOfDistance.position, _target.position);
            if (dist < _minDistanceToDisable)
            {
                gameObject.SetActive(false);
            }
            else
            {
                gameObject.SetActive(true);
                transform.position = _capsuleCollider.ClosestPointOnBounds(_target.transform.position);
                transform.LookAt(_target.transform.position);
            }
        }
    }

    public void SetCapsuleCollider(CapsuleCollider capsuleCollider) => _capsuleCollider = capsuleCollider;

    public void SetArrowPool(ObjectPool objectPool) => _arrowPool  = objectPool;

    public void SetTarget(Transform transform) => _target = transform;

    public void SetStartPointOfDistance(Transform transform) => _startPointOfDistance = transform;

    public void ReturnToPull()=> _arrowPool.ReturnObjectToPool(gameObject);
}
