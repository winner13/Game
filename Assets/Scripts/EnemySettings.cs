using UnityEngine;

public class EnemySettings: MonoBehaviour 
{
    [SerializeField] private ObjectPool _enemyPool;
    [SerializeField] private InGameEnemyCounter _enemyCounter;
    [SerializeField] private EnemyDifficulty _enemyDifficulty;

    public ObjectPool EnemyPool { get => _enemyPool;}

    public void SetEnemySettings(Enemy enemyScript)
    {
        SetFields(enemyScript);
        SetEnemyStats(enemyScript);
        IncreaseEnemyCount();
    }

    private void SetFields(Enemy enemy)
    {
        enemy.ObjectPool ??= _enemyPool;
        enemy.EnemyCounter ??= _enemyCounter;
    }

    private void SetEnemyStats(Enemy enemyScript)
    {   
        enemyScript.Health.SetStat(_enemyDifficulty.GetHealthValue());
        enemyScript.Speed.SetStat(_enemyDifficulty.GetSpeedValue());
    }

    private void IncreaseEnemyCount()
    {
        _enemyCounter.IncreaseEnemyCount();
    }
}
